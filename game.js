const player = require('./player');
const playBoard = require('./board');



    const winCombinations = [
        ['1:1', '1:2', '1:3'],
        ['2:1', '2:2', '2:3'],
        ['3:1', '3:2', '3:3'],
        ['1:1', '2:1', '3:1'],
        ['1:2', '2:2', '3:2'],
        ['1:3', '2:3', '3:3'],
        ['1:1', '2:2', '3:3'],
        ['1:3', '2:2', '3:1'],
    ];

    let firstPlayer = new player();
    let secondPlayer = new player();
    let board = new playBoard();

    let currentPlayer = firstPlayer;
    let currentStep = 0;

    

    theGame=()=>
    {
        process.stdout.write(`Enter the player names: \n`)
        board.createBoard();
    }
    
    drawTheBoard=()=>
    {
        for (r=0; r<3; r++)
        {
            for (c=0; c<3; c++) process.stdout.write(board.board[r][c]) 
            process.stdout.write(`\n`)
        }
    }


    process.stdin.on("data", data =>
    {
        if (firstPlayer.name === '') firstPlayer.name=data.toString().trim();
        else if (secondPlayer.name === '')
        {
            secondPlayer.name=data.toString().trim();
            process.stdout.write(`\n \n ${firstPlayer.name} vs. ${secondPlayer.name} \n\n Let's play! \n`);
            drawTheBoard();
            process.stdout.write(`\n ${currentPlayer.name} your turn: `);
        }
        else
        {
            data=data.toString().trim();
            if (data==='q') process.exit();
            else if (data.search(':')===-1 || isNaN(r=parseInt(data.split(':')[0])) || isNaN(c=parseInt(data.split(':')[1])) || r<1 || r>3 || c<1 || c>3 )
            {
                process.stdout.write(`${currentPlayer.name} you enter wrong field. Please use Row:Column format (Row and Column should be numeric) \n`);
                drawTheBoard();
                process.stdout.write(`\n ${currentPlayer.name} try again: `);
            }
            else 
            {
                if (board.board[r-1][c-1]!=='.')
                {
                    process.stdout.write (`The field ${r}:${c} is occupied. Please choose another field \n`);
                    drawTheBoard();
                    process.stdout.write(`\n ${currentPlayer.name} try again: `);
                }
                else step(r-1,c-1);
            }
        }
    })

    process.on('exit', (what)=>
    {
        if (what ==='victory')
        {        
            process.stdout.write(` ${currentPlayer.name} WON!!!\n\n`);
            drawTheBoard();
        }
        else if(what === 'draw') process.stdout.write(`Nobody WON!!!\n\n`);
        else process.stdout.write(` ${currentPlayer.name} Fail!!!\n\n`);
    })

    step=(r,c)=>{
        if (currentStep===8) 
        {
            process.exit('draw');
        }
        
        currentPlayer.steps.push((r+1)+':'+(c+1));

        if (currentPlayer===firstPlayer)
        {
            board.board[r][c]='X';
            checkVictory();
            currentPlayer=secondPlayer;
        }
        else {
            board.board[r][c]='O';
            checkVictory();
            currentPlayer=firstPlayer;
        }

        drawTheBoard();
        process.stdout.write(`\n ${currentPlayer.name} your turn: `)
        currentStep++;
    }

    checkVictory=()=>
    {
        for (let w = 0, wLen = winCombinations.length; w < wLen; w++) 
        {
            let someWinArr = winCombinations[w],
                count = 0;
            if (someWinArr.indexOf(currentPlayer.steps[currentPlayer.steps.length-1]) !== -1) 
            {
                for (var k = 0, kLen = someWinArr.length; k < kLen; k++) 
                {
                    if (currentPlayer.steps.indexOf(someWinArr[k]) !== -1)
                    {
                        count++;
                        if (count === 3) process.exit('victory');
                    }
                }
                count = 0;
            }
        }
    }

    theGame();